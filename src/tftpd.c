#include <stdio.h>
#include <stdarg.h>
#include <dirent.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <errno.h>
#include <signal.h>

// Type   Op #     Format without header
//
//           2 bytes    string   1 byte     string   1 byte
//           -----------------------------------------------
//    RRQ/  | 01/02 |  Filename  |   0  |    Mode    |   0  |
//    WRQ    -----------------------------------------------
//           2 bytes    2 bytes       n bytes
//           ---------------------------------
//    DATA  | 03    |   Block #  |    Data    |
//           ---------------------------------
//           2 bytes    2 bytes
//           -------------------
//    ACK   | 04    |   Block #  |
//           --------------------
//           2 bytes  2 bytes        string    1 byte
//           ----------------------------------------
//    ERROR | 05    |  ErrorCode |   ErrMsg   |   0  |
//           ----------------------------------------

//tftp types
#define RRQ 	1		// Read request
#define WRQ		2		// Write request
#define DATA	3		// Data package
#define ACK		4		// Acknowledgment package
#define ERROR 	5		// Error package

//error codes
#define ENOTDEF				0		// Not defined, see error message (if any).
#define EFILENOTFOUND		1		// File not found.
#define ENOACCESS			2		// Access violation.
#define ENOALLOC			3		// Disk full or allocation exceeded.
#define EILLEGALOP			4		// Illegal TFTP operation.
#define EUNKID				5		// Unknown transfer ID.
#define EFILEEXISTS			6		// File already exists.
#define ENOSUCHUSER			7		// No such user.

//general
#define PAYLOAD_SIZE 		512		//the default payload size of the data
#define PACKAGE_SEND_NEXT	0		//if a package was successfully sent
#define PACKAGE_ERROR		1		//if a package transmission ended in error
#define PACKAGE_RETRY 		2		//if the client asks for a re-send
#define PACKAGE_TIMEOUT 	3		//if the client doesn't respond
#define PACKAGE_DONE 		4		//if the server has finished to send the file

//the tftp package that is sent
struct tftp_package {
	//the type of the package
	uint16_t opcode;
	union{
		//RRQ/WRQ package
		struct r {
			uint8_t filename[PAYLOAD_SIZE];
		}request;
		//DATA package
		struct d {
			uint16_t block_nr;
			char data[PAYLOAD_SIZE];
		}data;
		//ACKNOWLEDGEMENT package
		struct a {
			uint16_t block_nr;
		}ack;
		//ERROR package
		struct e {
			uint16_t code;
			char message[PAYLOAD_SIZE];
		}error;
	};
};

///////////FUNCTIONS///////////
int sendpackage(int code, size_t bytesRead);
int handleACK(struct tftp_package *package);
int waitForResponse();
int interpreteInput(int argc, char *argv[]);
int validFilePath(char* file);
char* concat(const char *s1, const char *s2);
void cntrlC();
void message(const char* format, ... );
void initalizeUDP(int portNumber);
void handleRRQ(struct tftp_package *package);
void listenForConnection();
void sendErrorPackage(char* message, int code);
void sendAckPackage();
void cleanup();
void closeConnection();

///////GLOBAL PROEPERTIES///////
//store socketfd for convenience
static int socketfd;
//when we send fewer than 512 bytes mark the request as the last one
int lastRequest = 0; // 1 == true
//Store the block number
int blockNumber = 0;
//store our file descriptor
int fd = -1;
//if we are serving a client
unsigned int serverIsBusy = 0;
//socket address and server for convenience
struct sockaddr_in server, client;
//hold on to our last sent pakcet
struct tftp_package activePackage;
//hold on to the directory path requested
static char *currentDirectoryPath;
///////////////////////////////

///////////PROGRAM START///////////
int main(int argc, char *argv[]){
	//close on ctrl c
	signal(SIGINT, cntrlC);
	return interpreteInput(argc,argv);
}

//Interprets the input parameters to get the port ahd path to files
int interpreteInput(int argc, char *argv[]){
	//we must have valid arguments
	if (argc != 3){
		message("Invalid arguments. Must define a port and a directory");
		return EXIT_FAILURE;
	}
	//get the port number
	int port = atoi(argv[1]);
	//get the directory we want to retreive files from
	char *directoryPath = argv[2];
	//store the directory path that is asked for
	currentDirectoryPath = directoryPath;
	message("Starting the Server at port: %d and with directory: %s", port, currentDirectoryPath);
	//setup the udp connections and begin receiving
	initalizeUDP(port);
	//normal exit
	return EXIT_SUCCESS;
}

//starts the UDP socket and server
void initalizeUDP(int portNumber) {
    // Create and bind a UDP socket.
    socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	// Receives should timeout after 30 seconds.
	struct timeval timeout;
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;
	if (setsockopt(socketfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0){
		message("setting timeout error");
	}
	//allocate the server
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(portNumber);
	//bind
	if (bind(socketfd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) < 0){
		message("Error binding socket");
		return;
	}
	//wait for a request
	listenForConnection();
}

//constantly listen for requests
void listenForConnection(){
	while (1){
		//the package received from the client
		struct tftp_package package;
		socklen_t len = (socklen_t) sizeof(client);
		ssize_t n = recvfrom(socketfd, &package, sizeof(package), 0, (struct sockaddr *) &client, &len);
		//check for errors
		if (n < 0){
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				message("Client timed out");
				cleanup();
				continue;
			}
			else{
				message("Error on recvfrom");
				exit(EXIT_FAILURE);
				break;
			}
		}
		//if ther server is handling a client discard other results
		if (serverIsBusy != 0) continue;
		//store the clients address being served
		serverIsBusy = client.sin_addr.s_addr;
		//handle the operation being asked for
		switch (ntohs(package.opcode)) {
			case RRQ:
				handleRRQ(&package);
				break;
			case WRQ:
				sendErrorPackage("Uploading to the sever is forbidden", ENOACCESS);
				cleanup();
				break;
			default:
				sendErrorPackage("This request is forbidden at this time. Please start by sending a RRQ request.", EILLEGALOP);
				cleanup();
				break;
		}
	}
}

//reads the requested file and starts transmission
void handleRRQ(struct tftp_package *package){
	//get the file name requested
	char *filename = (char*)package->request.filename;
	//check if this is a valid file
	if (validFilePath(filename) == 0) {
		sendErrorPackage("File name is invalid because it leads outside of the allowed directory", ENOACCESS);
		return;
	}
	//print out the file being served
	message("file \"%s\" requested from %s:%d",filename, inet_ntoa(client.sin_addr), ntohs(client.sin_port));
	//get the full path of the file
	char *path = concat("./", currentDirectoryPath);
	char *pathWSlash = concat(path, "/");
	char *fullPath = concat(pathWSlash,filename);
	//store the status of the package sending
	int statusCode = PACKAGE_SEND_NEXT;
	//try to open the file
	if ((fd = open(fullPath, O_RDONLY)) >= 0){
		//read from the file 512 bytes in a row
		//in a continous manner and responde to erros and retries
		size_t bytesRead = 0;
		while ((bytesRead = read(fd, activePackage.data.data, PAYLOAD_SIZE)) > 0) {
			//send the package and wait for ack
			statusCode = sendpackage(statusCode, bytesRead);
			//stop if we finished or received an error
			if (statusCode == PACKAGE_DONE || statusCode == PACKAGE_ERROR || statusCode == PACKAGE_TIMEOUT){
				break;
			}
			else if (statusCode == PACKAGE_RETRY){
				while (statusCode == PACKAGE_RETRY){
					statusCode = sendpackage(statusCode, bytesRead);
				}
				if (statusCode == PACKAGE_DONE || statusCode == PACKAGE_ERROR || statusCode == PACKAGE_TIMEOUT){
					break;
				}
			}
			//clear the buffer with the file data for reuse
			memset(&activePackage.data.data[0], 0, PAYLOAD_SIZE);
		}
	}
	else{
		sendErrorPackage(strerror(errno), EFILENOTFOUND);
	}

	//finish the transmission if all went well
	if (statusCode == PACKAGE_DONE){
		sendAckPackage();
		//clear the buffer with the file data for reuse
		memset(&activePackage.data.data[0], 0, PAYLOAD_SIZE);
	}
	if (statusCode == PACKAGE_ERROR || statusCode == PACKAGE_TIMEOUT){
		//clear the buffer with the file data for reuse
		memset(&activePackage.data.data[0], 0, PAYLOAD_SIZE);
	}
	//we are done with the current file
	cleanup();
	free(path);
	free(pathWSlash);
	free(fullPath);
}

//sends and error to the client
void sendErrorPackage(char* message, int code) {
	struct tftp_package errorpackage;
	memset(&errorpackage, 0, sizeof(errorpackage));
    errorpackage.opcode = ntohs(ERROR);
    errorpackage.error.code = ntohs(code);
    strcpy(errorpackage.error.message, message);
    sendto(socketfd, &errorpackage, sizeof(errorpackage), 0, (struct sockaddr *)&client, sizeof(client));
}

//sends and acknowledgement packet to finish the transmission
void sendAckPackage() {
	struct tftp_package ackPackage;
	memset(&ackPackage, 0, sizeof(ackPackage));
    ackPackage.opcode = ntohs(ACK);
    ackPackage.ack.block_nr = ntohs(0);
    sendto(socketfd, &ackPackage, sizeof(ackPackage), 0, (struct sockaddr *)&client, sizeof(client));
}

//sends a package to the client with the file data of 512 bytes
int sendpackage(int code, size_t bytesRead){
	if (code == PACKAGE_SEND_NEXT){
		//increment the block number
		blockNumber++; //Starts from 0 so 1 is the first package
		activePackage.opcode = ntohs(DATA);
		activePackage.data.block_nr = ntohs(blockNumber);
	}
	//check if this is the last request e.g < 512 bytes
	if (bytesRead < PAYLOAD_SIZE){
		lastRequest = 1;
	}
	//get the size of the package
	size_t packageLength = bytesRead + sizeof(uint16_t) + sizeof(uint16_t);
	socklen_t len = (socklen_t) sizeof(client);
	//send the package
	int send = sendto(socketfd, &activePackage, packageLength, 0, (struct sockaddr *) &client, len);
	if (send < 0 || (((size_t) send) != packageLength)) {
			message("Failed to send package");
			return PACKAGE_ERROR;
	}
	//loop and wait for response
	return waitForResponse();
}

//send and receive
int waitForResponse(){
	//the package received from the client
	struct tftp_package package;
	socklen_t len = (socklen_t) sizeof(client);
	ssize_t n = recvfrom(socketfd, &package, sizeof(package), 0, (struct sockaddr *) &client, &len);
	//check for errors
	if (n < 0){
		if (errno == EAGAIN || errno == EWOULDBLOCK) {
			message("Client timed out");
			return PACKAGE_TIMEOUT;
		}
		else{
			message("Error on recvfrom");
			exit(EXIT_FAILURE);
		}
	}
	//ignore requests from other clients then the one we are serving
	if (serverIsBusy != client.sin_addr.s_addr) return waitForResponse();
	//Switch on the request
	switch (ntohs(package.opcode)) {
		case ACK: return handleACK(&package);
		case DATA:
			sendErrorPackage("Uploading data is not allowed", EILLEGALOP);
			return PACKAGE_ERROR;
		case RRQ:
		case WRQ:
		case ERROR: return PACKAGE_ERROR;
	}
	//just in case
	return PACKAGE_ERROR;
}

//validate the Acknowledgment package
int handleACK(struct tftp_package *package){
	//check if the block numbers match
	if (htons(package->ack.block_nr) == htons(activePackage.data.block_nr)){
		//if this is the last request being served
		if (lastRequest == 1){
			return PACKAGE_DONE;
		}
		return PACKAGE_SEND_NEXT;
	}
	//check if we should try again to send the last packet
	else if (htons(package->ack.block_nr) == (htons(activePackage.data.block_nr) - 1)){
		return PACKAGE_RETRY;
	}
	else{
		sendErrorPackage("ACK package received from client is invalid.", 0);
		return PACKAGE_ERROR;
	}
}

//close the open file and prepare for another connection
void cleanup(){
	//close the file if it is open
	if (fd != -1){
		close(fd);
		fd = -1;
	}
	serverIsBusy = 0;
	blockNumber = 0;
	lastRequest = 0;
}

//closes the connection to the server and file descriptor
void closeConnection(){
	close(socketfd);
	cleanup();
	message("Goodbye");
	exit(EXIT_SUCCESS);
}

//////////////////HELPER METHODS////////////////////
void cntrlC(){
	message("");
	closeConnection();
}

//prints out the arguments
void message(const char* format, ... ) {
    va_list vargs;
    va_start(vargs, format);
    vprintf(format, vargs);
    printf("\n");
    va_end(vargs);
}

//Concatinate two strings, must remember to free the result
char* concat(const char *s1, const char *s2){
    char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

//Validates that the file given is in the directory statet in the beginning
int validFilePath(char* file) {
	//start at beginning
    char* letter = file;
	//loop until hitting the terminating byte
    while (*letter != '\0') {
		//if it contains the characters .. it is invalid
        if ((*(letter + 1) == '.') && (*letter == '.') ) {
            return 0;
        }
        letter++;
    }
	//the file path is valid
    return 1;
}
